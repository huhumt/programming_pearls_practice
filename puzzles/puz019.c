/*********************************************************************************************
First Missing Positive

Given an unsorted integer array nums, find the smallest missing positive integer.
You must implement an algorithm that runs in O(n) time and uses constant extra space.

Example 1:
Input: nums = [1,2,0]
Output: 3

Example 2:
Input: nums = [3,4,-1,1]
Output: 2

Example 3:
Input: nums = [7,8,9,11,12]
Output: 1

Constraints:

    1 <= nums.length <= 5 * 105
    -231 <= nums[i] <= 231 - 1
**********************************************************************************************/

#include <stdint.h>
#include <string.h>
#include <stdio.h>

static inline uint8_t GET_BIT(uint32_t num, uint8_t mask)
{
    return ((num & (((uint32_t)1) << mask)) >> mask);
}

static inline uint32_t SET_BIT(uint32_t num, uint8_t mask)
{
    return (num | (((uint32_t)1) << mask));
}

uint32_t find_min_missing_pos_int(int32_t *parray, uint32_t len)
{
    uint32_t bitmap_array[16000]; // can max support 512000 data
    uint32_t i, j, index, mod;

    memset(bitmap_array, 0, sizeof(bitmap_array));
    bitmap_array[0] = 1; // avoid zero itself

    for (i = 0; i < len; i += 1)
    {
        index = parray[i] / 32;
        mod = parray[i] - index * 32;

        if (parray[i] > 0 && GET_BIT(bitmap_array[index], mod) == 0)
        {
            bitmap_array[index] = SET_BIT(bitmap_array[index], mod);
        }
    }

    for (i = 0; i < 16000; i += 1)
    {
        for (j = 0; j < 32; j += 1)
        {
            if (GET_BIT(bitmap_array[i], j) == 0)
            {
                return 32 * i + j;
            }
        }
    }

    return 0;
}

void main(void)
{
    int32_t test_array[][10] = {
        {1, 2, 0},
        {3, 4, -1, 1},
        {7, 8, 9, 11, 12},
        {9, 2, 3, 4, 5, 6, 7, 8, 8, 10},
    };
    uint8_t i, len = sizeof(test_array) / sizeof(test_array[0]);

    for (i = 0; i < len; i += 1)
    {
        printf("%u ", find_min_missing_pos_int(&test_array[i][0], 10));
    }
}
