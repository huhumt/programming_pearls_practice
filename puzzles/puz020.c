/**********************************************************************************
Trapping Rain Water
Given n non-negative integers representing an elevation map
where the width of each bar is 1, compute how much water it can trap after raining.

Example 1:

Input: height = [0,1,0,2,1,0,1,3,2,1,2,1]
Output: 6

Example 2:
Input: height = [4,2,0,3,2,5]
Output: 9

Constraints:
    n == height.length
    0 <= n <= 3 * 104
    0 <= height[i] <= 105
*************************************************************************************/

#include <stdint.h>
#include <stdio.h>

static inline uint32_t MIN(uint32_t x1, uint32_t x2)
{
    return (x1 > x2 ? x2 : x1);
}

uint32_t trap_rain_water(uint8_t *parray, uint32_t len)
{
    uint32_t trap_rain_water_sum = 0;
    uint32_t i, left_index = 0, tmp_sum;
    uint8_t left = 0;

    do
    {
        left = 0;

        for (i = left_index; i < len; i += 1)
        {
            if (left == 0)
            {
                if (parray[i] > 0)
                {
                    left = parray[i];
                    left_index = i;
                    tmp_sum = 0;
                }

                if (i >= len - 1)
                {
                    return trap_rain_water_sum;
                }
            }
            else
            {
                if (parray[i] >= left)
                {
                    if (i > left_index + 1)
                    {
                        trap_rain_water_sum += ((i - left_index - 1) * MIN(left, parray[i]) - tmp_sum);
                    }

                    left = parray[i];
                    left_index = i;
                    tmp_sum = 0;
                }
                else
                {
                    tmp_sum += parray[i];
                }
            }
        }
    } while (++left_index < len);

    return trap_rain_water_sum;
}

void main(void)
{
    uint8_t test_array[][20] =
    {
        {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1},
        {4, 2, 0, 3, 2, 5},
        {4, 2, 0, 3, 2, 4},
        {8, 2, 0, 3, 2, 4},
        {4, 2, 0, 8, 2, 4},
        {4, 8, 0, 3, 2, 4},
    };
    uint8_t i, j, len = sizeof(test_array) / sizeof(test_array[0]);
    uint32_t water_sum;

    for (i = 0; i < len; i += 1)
    {
        water_sum = trap_rain_water(&test_array[i][0], 20);
        printf("%d ", water_sum);
    }

    printf("\n\n");
}
