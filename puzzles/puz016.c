/******************************************************************************************
Add Two Numbers

You are given two non-empty linked lists representing two non-negative integers.
The digits are stored in reverse order, and each of their nodes contains a single digit.
Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example 1:

Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.

Example 2:

Input: l1 = [0], l2 = [0]
Output: [0]

Example 3:

Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
Output: [8,9,9,9,0,0,0,1]
**********************************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct node_t {
    uint8_t val;
    struct node_t *next;
};

struct node_t *add_two_node(struct node_t *num1, struct node_t *num2)
{
    uint8_t sum = 0;
    struct node_t *root_node = (struct node_t *)calloc(1, sizeof(struct node_t));
    struct node_t *tmp_node = root_node, *new_node;
    struct node_t *tmp1 = num1, *tmp2 = num2;

    while (tmp1 != NULL || tmp2 != NULL || sum > 0)
    {
        if (tmp1 != NULL)
        {
            sum += tmp1->val;
            tmp1 = tmp1->next;
        }

        if (tmp2 != NULL)
        {
            sum += tmp2->val;
            tmp2 = tmp2->next;
        }

        tmp_node->val = sum % 10;
        sum /= 10;

        if (tmp1 != NULL || tmp2 != NULL || sum > 0)
        {
            new_node = (struct node_t *)calloc(1, sizeof(struct node_t));
            tmp_node->next = new_node;
            tmp_node = new_node;
        }
        else
        {
            tmp_node->next = NULL;
        }
    }

    return root_node;
}

struct node_t* create_node(uint8_t *array, uint8_t len)
{
    uint8_t i;
    struct node_t *root_node, *pre_node, *cur_node;

    root_node = (struct node_t*)calloc(1, sizeof(struct node_t));
    pre_node = root_node;
    pre_node->val = array[0];
    for (i = 1; i < len; i += 1) {
        cur_node = (struct node_t*)calloc(1, sizeof(struct node_t));
        cur_node->val = array[i];
        pre_node->next = cur_node;
        pre_node = cur_node;
    }
    cur_node->next = NULL;

    return root_node;
}

void print_node(struct node_t* root_node)
{
    struct node_t* tmp_node = root_node;

    if (root_node == NULL) {
        return;
    }

    while (1) {
        printf("%d ", tmp_node->val);
        if (tmp_node->next == NULL) {
            printf("\n");
            return;
        } else {
            tmp_node = tmp_node->next;
        }
    }
}

void main(void)
{
    uint8_t array1[3] = {7, 8, 9};
    uint8_t array2[4] = {9, 5, 6, 3};

    struct node_t *node1 = create_node(array1, sizeof(array1) / sizeof(uint8_t));
    struct node_t *node2 = create_node(array2, sizeof(array2) / sizeof(uint8_t));
    print_node(node1);
    print_node(node2);
    struct node_t *sum_node = add_two_node(node1, node2);
    print_node(sum_node);
}
