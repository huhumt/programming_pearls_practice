/***************************************************************************
Valid Number

A valid number can be split up into these components (in order):
    A decimal number or an integer.
    (Optional) An 'e' or 'E', followed by an integer.

A decimal number can be split up into these components (in order):
    (Optional) A sign character (either '+' or '-').
    One of the following formats:
        One or more digits, followed by a dot '.'.
        One or more digits, followed by a dot '.', followed by one or more digits.
        A dot '.', followed by one or more digits.

An integer can be split up into these components (in order):
    (Optional) A sign character (either '+' or '-').
    One or more digits.

For example, all the following are valid numbers:
["2", "0089", "-0.1", "+3.14", "4.", "-.9", "2e10", "-90E3", "3e+7", "+6e-1", "53.5e93", "-123.456e789"]
while the following are not valid numbers: ["abc", "1a", "1e", "e3", "99e2.5", "--6", "-+3", "95a54e53"].

Given a string s, return true if s is a valid number.

Example 1: Input: s = "0" Output: true
Example 2: Input: s = "e" Output: false
Example 3: Input: s = "." Output: false
Example 4: Input: s = ".1" Output: true
**************************************************************************************************************/

#include <stdint.h>
#include <stdio.h>

uint8_t is_valid_number(char *pstr)
{
    uint8_t pos_neg_occur_flag = 0;
    uint8_t dot_occur_flag = 0;
    uint8_t e_occur_flag = 0;
    uint8_t digit_occur_flag = 0;
    uint8_t digit_before_e_flag = 0;
    uint8_t digit_after_e_flag = 0;

    while (*pstr != '\0')
    {
        if (*pstr == '+' || *pstr == '-')
        {
            if (pos_neg_occur_flag)
            {
                return 0;
            }

            pos_neg_occur_flag = 1;
        }
        else if (*pstr == '.')
        {
            if (e_occur_flag)
            {
                return 0;
            }

            dot_occur_flag = 1;
        }
        else if (*pstr == 'e' || *pstr == 'E')
        {
            if (e_occur_flag)
            {
                return 0;
            }

            e_occur_flag = 1;
            pos_neg_occur_flag = 0; // allow +/- again after e/E
        }
        else if (*pstr >= '0' && *pstr <= '9')
        {
            if (e_occur_flag)
            {
                digit_after_e_flag = 1;
            }
            else
            {
                digit_before_e_flag = 1;
            }

            digit_occur_flag = 1;
        }
        else
        {
            return 0;
        }

        pstr += 1;
    }

    if (!digit_occur_flag && (pos_neg_occur_flag || dot_occur_flag))
    {
        return 0;
    }

    if (e_occur_flag && (!digit_before_e_flag || !digit_after_e_flag))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

void main(void)
{
    char true_array[][20] = {"2", "0089", "-0.1", "+3.14", "4.", ".1", "-.9", "2e10", "-90E3", "3e+7", "+6e-1", "53.5e93", "-123.456e789"};
    char false_array[][20] = {"abc", "1a", "1e", "+", "e", ".", "e3", "99e2.5", "--6", "-+3", "95a54e53", "5.1e", "5.e"};
    uint8_t i, len;

    len = sizeof(true_array) / sizeof(true_array[0]);
    for (i = 0; i < len; i += 1)
    {
        printf("%d ", is_valid_number(true_array[i]));
    }
    printf("\n");

    len = sizeof(false_array) / sizeof(false_array[0]);
    for (i = 0; i < len; i += 1)
    {
        printf("%d ", is_valid_number(false_array[i]));
    }
    printf("\n");
}
