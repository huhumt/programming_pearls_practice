/***************************************************************************************
String to Integer (atoi)

Implement the myAtoi(string s) function, which converts a string
to a 32-bit signed integer (similar to C/C++'s atoi function).

The algorithm for myAtoi(string s) is as follows:
    Read in and ignore any leading whitespace.
    Check if the next character (if not already at the end of the string) is '-' or '+'.
    Read this character in if it is either. This determines if the final result is
    negative or positive respectively. Assume the result is positive if neither is present.
    Read in next the characters until the next non-digit charcter
    or the end of the input is reached. The rest of the string is ignored.
    Convert these digits into an integer (i.e. "123" -> 123, "0032" -> 32).
    If no digits were read, then the integer is 0. Change the sign as necessary (from step 2).
    If the integer is out of the 32-bit signed integer range [-231, 231 - 1],
    then clamp the integer so that it remains in the range.
    Specifically, integers less than -231 should be clamped to -231,
    and integers greater than 231 - 1 should be clamped to 231 - 1.
    Return the integer as the final result.

Note:
    Only the space character ' ' is considered a whitespace character.
    Do not ignore any characters other than the leading whitespace
    or the rest of the string after the digits.

Example 1: Input: s = "42" Output: 42
Example 2: Input: s = "   -42" Output: -42
Example 3: Input: s = "4193 with words" Output: 4193
Example 4: Input: s = "words and 987" Output: 0
Example 5: Input: s = "-91283472332" Output: -2147483648

Constraints:

    0 <= s.length <= 200
    s consists of English letters (lower-case and upper-case), digits (0-9), ' ', '+', '-', and '.'.
****************************************************************************************************************/

#include <stdint.h>
#include <stdio.h>

int32_t self_atoi(char *pstr)
{
    uint8_t pos_int_flag = 2, first_none_zero_flag = 0;
    uint8_t valid_digit_flag = 0;
    /* int32_t range from [-2147483648, 2147483648) */
    uint8_t digit_array[10], len = 0;
    int32_t rate = 1, output_int = 0;

    while (*pstr != '\0')
    {
        if (*pstr >= '0' && *pstr <= '9')
        {
            valid_digit_flag = 1;
            digit_array[len] = *pstr - '0';

            if (first_none_zero_flag == 0)
            {
                if (digit_array[len] > 0)
                {
                    first_none_zero_flag = 1;
                }
            }

            if (first_none_zero_flag && ++len > 10)
            {
                return pos_int_flag ? 2147483647 : -2147483648;
            }
        }
        else
        {
            if (pos_int_flag < 2 || valid_digit_flag)
            {
                break;
            }
            else
            {
                if (*pstr == '-')
                {
                    pos_int_flag = 0; // negetive intger
                }
                else if (*pstr == '+')
                {
                    pos_int_flag = 1; // positive intger
                }
                else if (*pstr == ' ')
                {
                    // do nothing, just ignore whitespace
                }
                else
                {
                    return 0;
                }
            }
        }

        pstr += 1;
    }

    while (len > 0)
    {
        if (pos_int_flag == 0)
        {
            output_int -= rate * digit_array[len - 1];
        }
        else
        {
            output_int += rate * digit_array[len - 1];
        }

        rate *= 10;
        len -= 1;
    }
    
    if (pos_int_flag > 0 && output_int < 0)
    {
        return 2147483647; // overflow
    }
    else if (pos_int_flag == 0 && output_int > 0)
    {
        return -2147483648; // overflow
    }
    else
    {
        return output_int;
    }
}

void main(void)
{
    printf("Input: s = '42'              Output: %d\n", self_atoi("42"));
    printf("Input: s = '000000000000042' Output: %d\n", self_atoi("000000000000042"));
    printf("Input: s = '00000000000+042' Output: %d\n", self_atoi("00000000000+042"));
    printf("Input: s = '-00000000000042' Output: %d\n", self_atoi("-00000000000042"));
    printf("Input: s = '00000000000-042' Output: %d\n", self_atoi("00000000000-042"));
    printf("Input: s = '00000000040+042' Output: %d\n", self_atoi("00000000040+042"));
    printf("Input: s = '00000000040-042' Output: %d\n", self_atoi("00000000040-042"));
    printf("Input: s = '   -42'          Output: %d\n", self_atoi("   -42"));
    printf("Input: s = ' +  42'          Output: %d\n", self_atoi(" +  42"));
    printf("Input: s = ' -  42'          Output: %d\n", self_atoi(" -  42"));
    printf("Input: s = ' + -42'          Output: %d\n", self_atoi(" + -42"));
    printf("Input: s = '4193 with words' Output: %d\n", self_atoi("4193 with words"));
    printf("Input: s = 'words and 987'   Output: %d\n", self_atoi("words and 987"));
    printf("Input: s = '-91283472332'    Output: %d\n", self_atoi("-91283472332"));
    printf("Input: s = '-2147483649'     Output: %d\n", self_atoi("-2147483649"));
    printf("Input: s = '-2147483648'     Output: %d\n", self_atoi("-2147483648"));
    printf("Input: s = '-2147483647'     Output: %d\n", self_atoi("-2147483647"));
    printf("Input: s = '2147483648'      Output: %d\n", self_atoi("2147483648"));
    printf("Input: s = '2147483647'      Output: %d\n", self_atoi("2147483647"));
    printf("Input: s = '2147483646'      Output: %d\n", self_atoi("2147483646"));
}
